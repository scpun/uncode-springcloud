<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

	<parent>
		<groupId>org.springframework.cloud</groupId>
		<artifactId>spring-cloud-build</artifactId>
		<version>2.0.2.RELEASE</version>
	</parent>


    <modelVersion>4.0.0</modelVersion>
	<groupId>cn.uncode</groupId>
    <artifactId>uncode-springcloud</artifactId>
    <packaging>pom</packaging>
    <version>0.0.5-SNAPSHOT</version>


    <name>uncode-springcloud</name>
    <modules>
		<module>uncode-springcloud-dependencies</module>
		<module>uncode-springcloud-utils</module>
		<module>uncode-springcloud-starter-boot</module>
		<module>uncode-springcloud-eureka</module>
        <module>uncode-springcloud-gateway</module>
		<module>uncode-springcloud-starter-web</module>
		<module>uncode-springcloud-starter-log</module>
		<module>uncode-springcloud-starter-bus</module>
		<module>uncode-springcloud-starter-fuse</module>
		<module>uncode-springcloud-starter-canary</module>
		<module>uncode-springcloud-starter-security</module>
		<module>uncode-springcloud-starter-monitor</module>
		<module>uncode-springcloud-parent</module>
		<module>uncode-springcloud-admin</module>
    </modules>
	
	<properties>
		<maven.test.skip>true</maven.test.skip>
		<maven.test.failure.ignore>true</maven.test.failure.ignore>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>1.8</java.version>
		<spring-cloud.version>Finchley.RELEASE</spring-cloud.version>
		<spring-boot.version>2.0.1.RELEASE</spring-boot.version>
		<spring-cloud-alibaba.version>0.2.1.RELEASE</spring-cloud-alibaba.version>
		<uncode-springcloud.version>${project.version}</uncode-springcloud.version>
        
        <!-- 推荐使用Harbor -->
        <docker.registry.url>10.1.43.101</docker.registry.url>
        <docker.registry.host>http://${docker.registry.url}:2375</docker.registry.host>
        <docker.plugin.version>1.2.0</docker.plugin.version>
    </properties>
	
	<dependencyManagement>
        <dependencies>
        	<dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
			<dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring-boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
			<dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>${spring-cloud-alibaba.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
		</dependencies>
    </dependencyManagement>

    <build>
    	<finalName>${project.name}</finalName>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
            <resource>
                <directory>src/main/java</directory>
                <includes>
                    <include>**/*.xml</include>
                </includes>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.0</version>
                <configuration>
                    <target>${java.version}</target>
                    <source>${java.version}</source>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
        </plugins>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <executions>
                        <execution>
                            <goals>
                                <goal>repackage</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>com.spotify</groupId>
                    <artifactId>docker-maven-plugin</artifactId>
                    <version>${docker.plugin.version}</version>
                    <configuration>
                        <forceTags>true</forceTags> <!--覆盖相同标签镜像-->
                        <imageName>${docker.registry.url}/uncode/${project.artifactId}:${project.version}</imageName> <!--指定镜像名称 仓库/镜像名:标签-->
                        <dockerDirectory>${project.basedir}</dockerDirectory>
                        <dockerHost>${docker.registry.host}</dockerHost> <!-- 指定仓库地址 -->
                        <resources>
                            <resource>                                            <!-- 指定资源文件 -->
                                <targetPath>/</targetPath>                        <!-- 指定要复制的目录路径，这里是当前目录 -->
                                <directory>${project.build.directory}</directory> <!-- 指定要复制的根目录，这里是target目录 -->
                                <include>${project.build.finalName}.jar</include> <!-- 指定需要拷贝的文件，这里指最后生成的jar包 -->
                            </resource>
                        </resources>
                        <registryUrl>${docker.registry.url}</registryUrl>
                        <serverId>${docker.registry.url}</serverId>
                        <pushImage>true</pushImage>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    
    
    <repositories>
        <repository>
            <id>aliyun-repos</id>
            <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>unidal-repos</id>
            <url>http://unidal.org/nexus/content/repositories/releases/</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>

    </repositories>

    <pluginRepositories>
        <pluginRepository>
            <id>aliyun-plugin</id>
            <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </pluginRepository>
    </pluginRepositories>

    <distributionManagement>
        <repository>
            <id>releases</id>
            <url>
                http://nexus.com/content/repositories/fgRelease/
            </url>
        </repository>
        <snapshotRepository>
            <id>snapshots</id>
            <url>
                http://nexus.com/content/repositories/fgSnapshots/
            </url>
        </snapshotRepository>
    </distributionManagement>
</project>