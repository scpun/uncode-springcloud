package cn.uncode.springcloud.admin.model.system.dto;

import java.util.Date;

import cn.uncode.dal.annotation.Field;
import cn.uncode.dal.annotation.Table;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * 数据库实体类,此类由Uncode自动生成
 * @author uncode
 * @date 2019-06-03
 */
@Data
@Table(name ="canary")
@ApiModel(description = "灰度信息")
public class CanaryDTO {
	public final static String TABLE_NAME = "canary";
	public final static String ID = "id";
	public final static String NAME = "name";
	public final static String FLAG = "flag";
	public final static String TYPE = "type";
	public final static String KEY = "key";
	public final static String VALUE = "value";
	public final static String CREATE_TIME = "create_time";
	public final static String UPDATE_TIME = "update_time";
	public final static String REMARK = "remark";

	/**
	 * 主键ID
	 */
	@ApiModelProperty(name = "ID")
	private Long id;
	/**
	 * 灰度名称
	 */
	@ApiModelProperty(name = "灰度名称")
	private String name;
	/**
	 * 灰度标识
	 */
	@ApiModelProperty(name = "灰度标识")
	private String flag;
	/**
	 * 类型（ip:网址，keyInRequest:请求标识，keyInSession:会话标识）
	 */
	@ApiModelProperty(name = "类型")
	private String type;
	/**
	 * 灰度关键字
	 */
	@ApiModelProperty(name = "灰度关键字")
	private String key;
	/**
	 * 灰度值
	 */
	@ApiModelProperty(name = "灰度值")
	private String value;
	/**
	 * 创建时间
	 */
	 @Field(name = "create_time")
	 @ApiModelProperty(name = "创建时间")
	private Date createTime;
	/**
	 * 更新时间
	 */
	 @Field(name = "update_time")
	 @ApiModelProperty(name = "更新时间")
	private Date updateTime;
	/**
	 * 备注
	 */
	@ApiModelProperty(name = "备注")
	private String remark;


}