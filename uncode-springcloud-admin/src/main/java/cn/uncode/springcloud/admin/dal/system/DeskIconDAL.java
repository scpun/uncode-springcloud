package cn.uncode.springcloud.admin.dal.system;

import cn.uncode.springcloud.admin.model.system.dto.DeskIconDTO;

import cn.uncode.dal.external.CommonDAL;
 /**
 * service接口类,此类由Uncode自动生成
 * @author uncode
 * @date 2019-05-14
 */
public interface DeskIconDAL extends CommonDAL<DeskIconDTO> {

}